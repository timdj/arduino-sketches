/*
 *   LightSensor.h - Library for flashing Morse code.
 *     Created by Thijs en Dave.
 * */
#ifndef DHTtempSensor_h
#define DHTtempSensor_h

#include "Arduino.h"
#include "Sensor.h"

class DHTtempSensor : public Sensor
{
    public:
        DHTtempSensor(int pin);
        int getValue();
    private:
        int _pin;
};

#endif
