#include <SPI.h>
#include "Arduino.h"
#include "ComEthernet.h"
#include <Ethernet.h>

EthernetClient client;

ComEthernet::ComEthernet(byte* mac, char* server, char* location) {
	_mac = mac;
	_server = server;
	_location = location;
}

void ComEthernet::comSetup() {
	delay(1000);
	Ethernet.begin(_mac);
}

void ComEthernet::comMaintain() {
	Ethernet.maintain();
	if (client.available()) {
	  char c = client.read();
	  Serial.write(c);
	}
}

bool ComEthernet::sendMessage(String message) {
	// close any connection before send a new request.
	// This will free the socket on the WiFi shield
	client.stop();

	// if there's a successful connection:
	if (client.connect(_server, 80)) {
		// send the HTTP GET request:
		client.print("GET ");
		client.print(_location);
		client.println(" HTTP/1.1");
		client.print("Host: ");
		client.println(_server);
		client.println("User-Agent: arduino-ethernet");
		client.println("Connection: close");
		client.println();
		return true;
	} 
	return false;
}

