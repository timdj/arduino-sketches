/*
 *   LightSensor.h - Library for flashing Morse code.
 *     Created by Thijs en Dave.
 * */
#ifndef LightSensor_h
#define LightSensor_h

#include "Arduino.h"
#include "Sensor.h"

class LightSensor : public Sensor
{
    public:
        LightSensor(int pin);
        int getValue();
    private:
        int _pin;
};

#endif
