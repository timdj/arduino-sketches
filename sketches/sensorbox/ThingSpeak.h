#include "Arduino.h"
#ifndef EndPoint_h
#define EndPoint_h

class ThingSpeak
{
  public:
    ThingSpeak(char* apiKey);
    String buildMessage(char* sensorValues);
    
  private:
    char* _apiKey;
};

#endif
