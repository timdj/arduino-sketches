#include "Arduino.h"
#ifndef ComWifi_h
#define ComWifi_h

class ComWifi
{
  public:
    ComWifi(char* ssid, char* pass, int* security, char* server, char* location);
    void comSetup();
    void comMaintain();
    bool sendMessage(String message);
    
  private:

    char* _ssid;
    char* _pass;
    int* _security;
    char* _server;
    char* _location;
    char* _url;
    bool displayConnectionDetails(void);
   
};

#endif
