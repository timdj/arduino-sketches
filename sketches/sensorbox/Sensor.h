/*
 *   Sensor.h - Library for flashing Morse code.
 *     Created by Thijs en Dave.
 * */
#ifndef Sensor_h
#define Sensor_h

#include "Arduino.h"

class Sensor
{
    public:
        Sensor();
        virtual int getValue();
};

#endif
