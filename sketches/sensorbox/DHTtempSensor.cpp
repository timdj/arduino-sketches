/*
 *   LightSensor.cpp - Library for flashing Morse code.
 *     Created by David A. Mellis, November 2, 2007.
 *       Released into the public domain.
 *       */

#include "Arduino.h"
#include "DHTtempSensor.h"

//Libraries
#include <DHT.h>

  //Constants
#define DHTPIN 2     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino


DHTtempSensor::DHTtempSensor(int pin):Sensor()
{
  Serial.print("DHTtempSensor_h init......");
  _pin = pin;
  Serial.println(_pin);


 
 dht.begin();
  
}

int DHTtempSensor::getValue()
{
    int temp = dht.readTemperature();
    return temp;
}
