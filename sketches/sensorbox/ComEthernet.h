#include "Arduino.h"
#ifndef ComEthernet_h
#define ComEthernet_h

class ComEthernet
{
  public:
    ComEthernet(byte* mac, char* server, char* location);
    void comSetup();
    void comMaintain();
    bool sendMessage(String message);
    
  private:
    byte* _mac;
    char* _server;
    char* _location;
};

#endif
