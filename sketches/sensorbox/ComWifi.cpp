

#include "Arduino.h"
#include "ComWifi.h"

#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <SPI.h>
#include <string.h>
#include "utility/debug.h"


// These are the interrupt and control pins
#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
// These can be any two pins
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
// Use hardware SPI for the remaining pins
// On an UNO, SCK = 13, MISO = 12, and MOSI = 11
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
                                         SPI_CLOCK_DIVIDER); // you can change this clock speed

#define IDLE_TIMEOUT_MS  3000      // Amount of time to wait (in milliseconds) with no data 
                                   // received before closing the connection.  If you know the server
                                   // you're accessing is quick to respond, you can reduce this value.



uint32_t ip;

/*
 * security
 *  WLAN_SEC_UNSEC  = 0
 *  WLAN_SEC_WEP = 1
 *  WLAN_SEC_WPA = 2
 *  WLAN_SEC_WPA2 = 3
 */
ComWifi::ComWifi(char* ssid, char* pass, int* security, char* server, char* location) {    
   Serial.begin(9600);
     Serial.println("start ComWifi");
  _ssid = ssid;
  _pass = pass;
  _security = security;
  _server = server;
  _location = location;
     
}

void ComWifi::comSetup() {

  /* Initialise the module */
  Serial.print("Start WiFi...");
  if (!cc3000.begin())
  {
    Serial.println("Couldn't begin()! Check your wiring?");
    while(1);
  }
 

  /* Connext to network */
  Serial.print("\nTry Connecting to: "); Serial.println(_ssid); Serial.println("...");
/*
     Serial.print("ssid");
  Serial.print(_ssid);
    Serial.println("pass");
   Serial.println(_pass);
     Serial.println("security");
 */

       
  if (!cc3000.connectToAP(_ssid, _pass, 3)) {
    Serial.println(F("Failed!"));
    while(1);
  }
   
  Serial.print(F("Hooray connected!"));
  
  /* Wait for DHCP to complete */
  while (!cc3000.checkDHCP())
  {
    delay(100); 
  }

   while (!displayConnectionDetails()) {
    delay(1000);
  }
}


bool ComWifi::displayConnectionDetails(void)
{
  uint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;
  
  if(!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv))
  {
    Serial.println(F("Unable to retrieve the IP Address!\r\n"));
    return false;
  }
  else
  {
    Serial.print(F("\nIP Addr: ")); cc3000.printIPdotsRev(ipAddress);
    Serial.print(F("\nNetmask: ")); cc3000.printIPdotsRev(netmask);
    Serial.print(F("\nGateway: ")); cc3000.printIPdotsRev(gateway);
    Serial.print(F("\nDHCPsrv: ")); cc3000.printIPdotsRev(dhcpserv);
    Serial.print(F("\nDNSserv: ")); cc3000.printIPdotsRev(dnsserv);
    Serial.println();
    return true;
  }
}




void ComWifi::comMaintain() {
   
}

bool ComWifi::sendMessage(String message) {

  Serial.println(" sendMessage");

  String domain = String(_server);
  String location = String(_location);
  String __url = domain + location;

  const char* hostname = _server;
  ip = 0;

        
  Serial.print("url"); 
  Serial.print(_url); 
  
  // Try looking up the website's IP address
    
  Serial.print(_server); 
  Serial.print(F(" -> "));
  while (ip == 0) {
    if (! cc3000.getHostByName(_server, &ip)) {
      Serial.println("Couldn't resolve!");
    }
    delay(500);
  }

   Serial.print(ip);

 cc3000.printIPdotsRev(ip);
  
 Adafruit_CC3000_Client www = cc3000.connectTCP(ip, 80);
  if (www.connected()) {
    www.fastrprint(F("GET "));
    www.fastrprint(_location);
    www.fastrprint(F(" HTTP/1.1\r\n"));
    www.fastrprint(F("Host: ")); www.fastrprint(_server); www.fastrprint(F("\r\n"));
    www.fastrprint(F("\r\n"));
    www.println();

    return true;
  } else {
    Serial.println("Connection failed");    
    return false;
  }
  
}


