/*
 *   LightSensor.cpp - Library for flashing Morse code.
 *     Created by David A. Mellis, November 2, 2007.
 *       Released into the public domain.
 *       */

#include "Arduino.h"
#include "LightSensor.h"

LightSensor::LightSensor(int pin):Sensor()
{
  Serial.print("Light init......");
  _pin = pin;
  Serial.println(_pin);
}

int LightSensor::getValue()
{
    int LDRValue = analogRead(_pin);
    return LDRValue;
}
