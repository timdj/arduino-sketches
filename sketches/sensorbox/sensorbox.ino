#include "Sensor.h"
#include "LightSensor.h"
#include "DHTtempSensor.h"

#define COMMODULE 2  //1 ethernet. 2 wifi , 3 zwave
#define DEBUG 1 // Serial output for test. Comment out for live
#define ENDPOINT 1 //1 thingspeak, 2 ?

#define SENSORCOUNT 2 // Number of connected sensors.

#define BOXNAME huiskamer
#define LOOPDELAY 5000 //wait x seconds. 5 seconds for testing.


/*
 * {type, pin, value }
 * 
 * Types:
 * LightSensor: 1
 */
int connectedSensors[][3] = { { 1, 0, 0 } , { 2, 2, 0 } };

Sensor* sensors[SENSORCOUNT];

#if ENDPOINT == 1 // Thingspeak
#include "ThingSpeak.h"
  char server[] = "api.thingspeak.com";
  char location[] = "/update?api_key=7E0L12YOJR7X3JSV";
  ThingSpeak endpoint();
#endif

#if COMMODULE == 1 // Ethernet
#include "ComEthernet.h"
  byte mac[] = {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
  };
  ComEthernet commodule(mac, server, location);
#endif


#if COMMODULE == 2 // Wifi
#include "ComWifi.h"
  char ssid[] = "Fuzeo";
  char pass[] = "Fuz30w1f1!";
  int security = 3;
  
  ComWifi commodule(ssid, pass, security, server, location);
#endif




void setup() {
   Serial.begin(9600);

  #ifdef DEBUG
    delay(500);
    Serial.begin(9600);
    Serial.println("started");
  #endif

  setupSensors();
     
  #if COMMODULE == 1
    Serial.println("ethernet");
  #endif
  #if COMMODULE == 2
    Serial.println("wifi");
  #endif

  Serial.println("Lets setup communication..");
  
//  commodule.comSetup();

  Serial.println("Communication is up, lets loop!");
  String sensorUnits[] = {"C", "F", "KM"};

  // setup netwerk / endpoint koppeling
}

void setupSensors() {
  // setup voor alle sensoren
   for (int i = 0; i < SENSORCOUNT; i++) {
    int pin = connectedSensors[i][1];
    char sensorType = connectedSensors[i][0];
    switch(sensorType) {
      case 1:
          sensors[i] = new LightSensor(pin);
          break;
      case 2:
          sensors[i] = new DHTtempSensor(pin);
          break;
      default:
         Serial.println("Unknown sensor type");
         Serial.print(sensorType);
         break;
    }
   }
}

void loop() {
  // put your main code here, to run repeatedly:
  // run eventuele code benodigd voor  com module (renew dhcp etc)
  // commodule.comMaintain();
  // zet sensoren aan
  // check op rdy
  // lees waardes
  // verstuur waardes
  // sleep ivm stroomverbruik
  readSensors();
  buildMessage();

  sendMessage("Test"); //test
  delay(LOOPDELAY); // wait 5 seconds before next loop.
}

// roept send aan van comm. module
bool sendMessage(String msg) {
//  commodule.sendMessage(msg);
}

void readSensors() {
    // vraag van alle sensoren om hun data
    // sensor leest uit en geeft value terug
    for (int i=0; i < SENSORCOUNT; i++) {
      connectedSensors[i][2] = sensors[i]->getValue();
    }
}

// deze zit in endpoint (die bepaald hoe hij zn bericht wil opmaken)
String buildMessage() {
  Serial.println("---------- Sensor data --------");
  for (int i = 0; i < SENSORCOUNT; i++) {
    Serial.print("Value from pin:");
    Serial.print(connectedSensors[i][1]);
    Serial.print("=");
    Serial.println(connectedSensors[i][2]);
  }
  // combineert sensor info (type, unit, value) naar... js
}




